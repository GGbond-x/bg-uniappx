import $time from '@/common/lib/time.js';

const useFormatTime = ()=>{
  const formatTime = (value) => $time.gettime(value)
  return {
    formatTime
  }
}

export default useFormatTime