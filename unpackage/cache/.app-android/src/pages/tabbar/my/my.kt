@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME", "UNUSED_ANONYMOUS_PARAMETER")
package uni.UNIuniappx;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenPagesTabbarMyMy : BasePage {
    constructor(__ins: ComponentInternalInstance) : super(__ins) {}
    companion object {
        @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
        var setup: (__props: GenPagesTabbarMyMy) -> Any? = fun(__props): Any? {
            val __ins = getCurrentInstance()!!;
            val _ctx = __ins.proxy as GenPagesTabbarMyMy;
            val _cache = __ins.renderCache;
            return fun(): Any? {
                return createElementVNode("view", null, utsArrayOf(
                    createVNode(unref(GenCompomentsGviewGNavbarClass), utsMapOf("bg-color" to "bg-white"), utsMapOf("right" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createVNode(unref(GenCompomentsGviewGIconButtonClass), utsMapOf("color" to "#000", "icon" to "\ue603"))
                        );
                    }
                    ), "_" to 1)),
                    createVNode(unref(GenCompomentsGviewGListitemClass), utsMapOf("image-size" to 120, "img-url" to "/static/images/avatar.jpg", "title" to "XXX", "is-show-right" to true), utsMapOf("rightCtx" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "iconfont font-md text-light-muted pr-3", "icon" to "\ue6bb"))
                        );
                    }
                    ), "default" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "flex flex-column"), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text-dark font-sm font-weight-bold"), "xxxx"),
                                createElementVNode("text", utsMapOf("class" to "text-light-muted font-md mt-2"), "八微号：123")
                            ))
                        );
                    }
                    ), "_" to 1)),
                    createVNode(unref(GenCompomentsGviewGDividerClass)),
                    createVNode(unref(GenCompomentsGviewGListitemClass), utsMapOf("title" to "支付", "is-show-right" to true), utsMapOf("icon" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "iconfont font-lg py-1 icon-plus-circle"))
                        );
                    }
                    ), "_" to 1)),
                    createVNode(unref(GenCompomentsGviewGDividerClass)),
                    createVNode(unref(GenCompomentsGviewGListitemClass), utsMapOf("title" to "收藏", "is-show-right" to true), utsMapOf("icon" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "iconfont font-lg py-1 icon-collection"))
                        );
                    }
                    ), "_" to 1)),
                    createVNode(unref(GenCompomentsGviewGListitemClass), utsMapOf("title" to "相册", "is-show-right" to true), utsMapOf("icon" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "iconfont font-lg py-1 icon-photo-color"))
                        );
                    }
                    ), "_" to 1)),
                    createVNode(unref(GenCompomentsGviewGDividerClass)),
                    createVNode(unref(GenCompomentsGviewGListitemClass), utsMapOf("title" to "表情", "is-show-right" to true), utsMapOf("icon" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "iconfont font-lg py-1 icon-emoji"))
                        );
                    }
                    ), "_" to 1)),
                    createVNode(unref(GenCompomentsGviewGListitemClass), utsMapOf("title" to "设置", "is-show-right" to true), utsMapOf("icon" to withSlotCtx(fun(): UTSArray<Any> {
                        return utsArrayOf(
                            createElementVNode("text", utsMapOf("class" to "iconfont font-lg py-1 icon-setting"))
                        );
                    }
                    ), "_" to 1))
                ));
            }
            ;
        }
        ;
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(), utsArrayOf(
                    GenApp.styles
                ));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf());
        var propsNeedCastKeys: UTSArray<String> = utsArrayOf();
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
