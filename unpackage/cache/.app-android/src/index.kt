@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME", "UNUSED_ANONYMOUS_PARAMETER")
package uni.UNIuniappx;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.loadFontFace as uni_loadFontFace;
open class GenApp : BaseApp {
    constructor(__ins: ComponentInternalInstance) : super(__ins) {
        onLaunch(fun(_: OnLaunchOptions) {
            uni_loadFontFace(LoadFontFaceOptions(family = "iconfont", global = true, source = "url(\"/static/iconfont.ttf\")"));
        }
        , __ins);
        onAppShow(fun(_: OnShowOptions) {
            console.log("App Show", " at App.uvue:12");
        }
        , __ins);
        onHide(fun() {
            console.log("App Hide", " at App.uvue:15");
        }
        , __ins);
    }
    companion object {
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0,
                    styles1,
                    styles2,
                    styles3
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("page" to padStyleMapOf(utsMapOf("backgroundColor" to "#EDEDED", "overflow" to "hidden")), "main-bg-color" to padStyleMapOf(utsMapOf("backgroundColor" to "#08C060")), "main-bg-hover-color" to padStyleMapOf(utsMapOf("backgroundColor" to "#08d869")), "main-text-color" to padStyleMapOf(utsMapOf("color" to "#08C060")), "border-main" to padStyleMapOf(utsMapOf("!borderColor" to "#08C060")), "bg-chat-item" to padStyleMapOf(utsMapOf("backgroundColor" to "#6BEE68")), "text-chat-item" to padStyleMapOf(utsMapOf("color" to "#6BEE68")), "navbar-bgcolor" to padStyleMapOf(utsMapOf("backgroundColor" to "#3875C6")), "iconfont" to padStyleMapOf(utsMapOf("!fontFamily" to "iconfont", "fontSize" to 16, "fontStyle" to "normal", "WebkitFontSmoothing" to "antialiased", "MozOsxFontSmoothing" to "grayscale")), "view" to padStyleMapOf(utsMapOf("fontSize" to "28rpx", "lineHeight" to 1.8, "color" to "#0E151D")), "text" to padStyleMapOf(utsMapOf("fontSize" to "28rpx", "lineHeight" to 1.8, "color" to "#0E151D")), "w-100" to padStyleMapOf(utsMapOf("width" to "750rpx")), "row" to padStyleMapOf(utsMapOf("display" to "flex", "flexWrap" to "wrap", "flexDirection" to "row", "justifyContent" to "flex-start")), "col-1" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "62.5rpx")), "col-2" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "125rpx")), "col-3" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "187.5rpx")), "col-4" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "250rpx")), "col-5" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "312.5rpx")), "col-6" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "375rpx")), "col-7" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "437.5rpx")), "col-8" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "500rpx")), "col-9" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "562.5rpx")), "col-10" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "625rpx")), "col-11" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "687.5rpx")), "col-12" to padStyleMapOf(utsMapOf("position" to "relative", "paddingRight" to "20rpx", "paddingLeft" to "20rpx", "width" to "750rpx")), "col-offset-12" to padStyleMapOf(utsMapOf("marginLeft" to "750rpx")), "col-offset-11" to padStyleMapOf(utsMapOf("marginLeft" to "687.5rpx")), "col-offset-10" to padStyleMapOf(utsMapOf("marginLeft" to "625rpx")), "col-offset-9" to padStyleMapOf(utsMapOf("marginLeft" to "562.5rpx")), "col-offset-8" to padStyleMapOf(utsMapOf("marginLeft" to "500rpx")), "col-offset-7" to padStyleMapOf(utsMapOf("marginLeft" to "437.5rpx")), "col-offset-6" to padStyleMapOf(utsMapOf("marginLeft" to "375rpx")), "col-offset-5" to padStyleMapOf(utsMapOf("marginLeft" to "312.5rpx")), "col-offset-4" to padStyleMapOf(utsMapOf("marginLeft" to "250rpx")), "col-offset-3" to padStyleMapOf(utsMapOf("marginLeft" to "187.5rpx")), "col-offset-2" to padStyleMapOf(utsMapOf("marginLeft" to "125rpx")), "col-offset-1" to padStyleMapOf(utsMapOf("marginLeft" to "62.5rpx")), "col-offset-0" to padStyleMapOf(utsMapOf("marginLeft" to 0)), "flex" to padStyleMapOf(utsMapOf("display" to "flex", "flexDirection" to "row")), "flex-row" to padStyleMapOf(utsMapOf("!flexDirection" to "row")), "flex-column" to padStyleMapOf(utsMapOf("!flexDirection" to "column")), "flex-row-reverse" to padStyleMapOf(utsMapOf("!flexDirection" to "row-reverse")), "flex-column-reverse" to padStyleMapOf(utsMapOf("!flexDirection" to "column-reverse")), "flex-wrap" to padStyleMapOf(utsMapOf("flexWrap" to "wrap")), "flex-nowrap" to padStyleMapOf(utsMapOf("flexWrap" to "nowrap")), "justify-start" to padStyleMapOf(utsMapOf("justifyContent" to "flex-start")), "justify-end" to padStyleMapOf(utsMapOf("justifyContent" to "flex-end")), "justify-between" to padStyleMapOf(utsMapOf("justifyContent" to "space-between")), "justify-center" to padStyleMapOf(utsMapOf("justifyContent" to "center")), "align-center" to padStyleMapOf(utsMapOf("alignItems" to "center")), "align-stretch" to padStyleMapOf(utsMapOf("alignItems" to "stretch")), "align-start" to padStyleMapOf(utsMapOf("alignItems" to "flex-start")), "align-end" to padStyleMapOf(utsMapOf("alignItems" to "flex-end")), "content-start" to padStyleMapOf(utsMapOf("alignContent" to "flex-start")), "content-end" to padStyleMapOf(utsMapOf("alignContent" to "flex-end")), "content-center" to padStyleMapOf(utsMapOf("alignContent" to "center")), "content-between" to padStyleMapOf(utsMapOf("alignContent" to "space-between")), "content-around" to padStyleMapOf(utsMapOf("alignContent" to "space-around")), "content-stretch" to padStyleMapOf(utsMapOf("alignContent" to "stretch")), "flex-1" to padStyleMapOf(utsMapOf("flex" to 1)), "flex-2" to padStyleMapOf(utsMapOf("flex" to 2)), "flex-3" to padStyleMapOf(utsMapOf("flex" to 3)), "flex-4" to padStyleMapOf(utsMapOf("flex" to 4)), "flex-5" to padStyleMapOf(utsMapOf("flex" to 5)), "flex-shrink" to padStyleMapOf(utsMapOf("flexShrink" to 0)), "container" to padStyleMapOf(utsMapOf("paddingRight" to "20rpx", "paddingLeft" to "20rpx")), "m-0" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginRight" to 0, "marginBottom" to 0, "marginLeft" to 0)), "m-auto" to padStyleMapOf(utsMapOf("marginTop" to "auto", "marginRight" to "auto", "marginBottom" to "auto", "marginLeft" to "auto")), "m-1" to padStyleMapOf(utsMapOf("marginTop" to "10rpx", "marginRight" to "10rpx", "marginBottom" to "10rpx", "marginLeft" to "10rpx")), "m-2" to padStyleMapOf(utsMapOf("marginTop" to "20rpx", "marginRight" to "20rpx", "marginBottom" to "20rpx", "marginLeft" to "20rpx")), "m-3" to padStyleMapOf(utsMapOf("marginTop" to "30rpx", "marginRight" to "30rpx", "marginBottom" to "30rpx", "marginLeft" to "30rpx")), "m-4" to padStyleMapOf(utsMapOf("marginTop" to "40rpx", "marginRight" to "40rpx", "marginBottom" to "40rpx", "marginLeft" to "40rpx")), "m-5" to padStyleMapOf(utsMapOf("marginTop" to "50rpx", "marginRight" to "50rpx", "marginBottom" to "50rpx", "marginLeft" to "50rpx")), "mt-0" to padStyleMapOf(utsMapOf("marginTop" to 0)), "mt-auto" to padStyleMapOf(utsMapOf("marginTop" to "auto")), "mt-1" to padStyleMapOf(utsMapOf("marginTop" to "10rpx")), "mt-2" to padStyleMapOf(utsMapOf("marginTop" to "20rpx")), "mt-3" to padStyleMapOf(utsMapOf("marginTop" to "30rpx")), "mt-4" to padStyleMapOf(utsMapOf("marginTop" to "40rpx")), "mt-5" to padStyleMapOf(utsMapOf("marginTop" to "50rpx")), "mb-0" to padStyleMapOf(utsMapOf("marginBottom" to 0)), "mb-auto" to padStyleMapOf(utsMapOf("marginBottom" to "auto")), "mb-1" to padStyleMapOf(utsMapOf("marginBottom" to "10rpx")), "mb-2" to padStyleMapOf(utsMapOf("marginBottom" to "20rpx")), "mb-3" to padStyleMapOf(utsMapOf("marginBottom" to "30rpx")), "mb-4" to padStyleMapOf(utsMapOf("marginBottom" to "40rpx")), "mb-5" to padStyleMapOf(utsMapOf("marginBottom" to "50rpx")), "ml-0" to padStyleMapOf(utsMapOf("marginLeft" to 0)), "ml-auto" to padStyleMapOf(utsMapOf("marginLeft" to "auto")), "ml-1" to padStyleMapOf(utsMapOf("marginLeft" to "10rpx")), "ml-2" to padStyleMapOf(utsMapOf("marginLeft" to "20rpx")), "ml-3" to padStyleMapOf(utsMapOf("marginLeft" to "30rpx")), "ml-4" to padStyleMapOf(utsMapOf("marginLeft" to "40rpx")), "ml-5" to padStyleMapOf(utsMapOf("marginLeft" to "50rpx")), "mr-0" to padStyleMapOf(utsMapOf("marginRight" to 0)), "mr-auto" to padStyleMapOf(utsMapOf("marginRight" to "auto")), "mr-1" to padStyleMapOf(utsMapOf("marginRight" to "10rpx")), "mr-2" to padStyleMapOf(utsMapOf("marginRight" to "20rpx")), "mr-3" to padStyleMapOf(utsMapOf("marginRight" to "30rpx")), "mr-4" to padStyleMapOf(utsMapOf("marginRight" to "40rpx")));
            }
        val styles1: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("mr-5" to padStyleMapOf(utsMapOf("marginRight" to "50rpx")), "my-0" to padStyleMapOf(utsMapOf("marginTop" to 0, "marginBottom" to 0)), "my-auto" to padStyleMapOf(utsMapOf("marginTop" to "auto", "marginBottom" to "auto")), "my-1" to padStyleMapOf(utsMapOf("marginTop" to "10rpx", "marginBottom" to "10rpx")), "my-2" to padStyleMapOf(utsMapOf("marginTop" to "20rpx", "marginBottom" to "20rpx")), "my-3" to padStyleMapOf(utsMapOf("marginTop" to "30rpx", "marginBottom" to "30rpx")), "my-4" to padStyleMapOf(utsMapOf("marginTop" to "40rpx", "marginBottom" to "40rpx")), "my-5" to padStyleMapOf(utsMapOf("marginTop" to "50rpx", "marginBottom" to "50rpx")), "mx-0" to padStyleMapOf(utsMapOf("marginLeft" to 0, "marginRight" to 0)), "mx-auto" to padStyleMapOf(utsMapOf("marginLeft" to "auto", "marginRight" to "auto")), "mx-1" to padStyleMapOf(utsMapOf("marginLeft" to "10rpx", "marginRight" to "10rpx")), "mx-2" to padStyleMapOf(utsMapOf("marginLeft" to "20rpx", "marginRight" to "20rpx")), "mx-3" to padStyleMapOf(utsMapOf("marginLeft" to "30rpx", "marginRight" to "30rpx")), "mx-4" to padStyleMapOf(utsMapOf("marginLeft" to "40rpx", "marginRight" to "40rpx")), "mx-5" to padStyleMapOf(utsMapOf("marginLeft" to "50rpx", "marginRight" to "50rpx")), "p-0" to padStyleMapOf(utsMapOf("paddingTop" to 0, "paddingRight" to 0, "paddingBottom" to 0, "paddingLeft" to 0)), "p" to padStyleMapOf(utsMapOf("paddingTop" to "5rpx", "paddingRight" to "5rpx", "paddingBottom" to "5rpx", "paddingLeft" to "5rpx")), "p-1" to padStyleMapOf(utsMapOf("paddingTop" to "10rpx", "paddingRight" to "10rpx", "paddingBottom" to "10rpx", "paddingLeft" to "10rpx")), "p-2" to padStyleMapOf(utsMapOf("paddingTop" to "20rpx", "paddingRight" to "20rpx", "paddingBottom" to "20rpx", "paddingLeft" to "20rpx")), "p-3" to padStyleMapOf(utsMapOf("paddingTop" to "30rpx", "paddingRight" to "30rpx", "paddingBottom" to "30rpx", "paddingLeft" to "30rpx")), "p-4" to padStyleMapOf(utsMapOf("paddingTop" to "40rpx", "paddingRight" to "40rpx", "paddingBottom" to "40rpx", "paddingLeft" to "40rpx")), "p-5" to padStyleMapOf(utsMapOf("paddingTop" to "50rpx", "paddingRight" to "50rpx", "paddingBottom" to "50rpx", "paddingLeft" to "50rpx")), "pt-0" to padStyleMapOf(utsMapOf("paddingTop" to 0)), "pt" to padStyleMapOf(utsMapOf("paddingTop" to "5rpx")), "pt-1" to padStyleMapOf(utsMapOf("paddingTop" to "10rpx")), "pt-2" to padStyleMapOf(utsMapOf("paddingTop" to "20rpx")), "pt-3" to padStyleMapOf(utsMapOf("paddingTop" to "30rpx")), "pt-4" to padStyleMapOf(utsMapOf("paddingTop" to "40rpx")), "pt-5" to padStyleMapOf(utsMapOf("paddingTop" to "50rpx")), "pb-0" to padStyleMapOf(utsMapOf("paddingBottom" to 0)), "pb-1" to padStyleMapOf(utsMapOf("paddingBottom" to "10rpx")), "pb" to padStyleMapOf(utsMapOf("paddingBottom" to "5rpx")), "pb-2" to padStyleMapOf(utsMapOf("paddingBottom" to "20rpx")), "pb-3" to padStyleMapOf(utsMapOf("paddingBottom" to "30rpx")), "pb-4" to padStyleMapOf(utsMapOf("paddingBottom" to "40rpx")), "pb-5" to padStyleMapOf(utsMapOf("paddingBottom" to "50rpx")), "pl-0" to padStyleMapOf(utsMapOf("paddingLeft" to 0)), "pl" to padStyleMapOf(utsMapOf("paddingLeft" to "5rpx")), "pl-1" to padStyleMapOf(utsMapOf("paddingLeft" to "10rpx")), "pl-2" to padStyleMapOf(utsMapOf("paddingLeft" to "20rpx")), "pl-3" to padStyleMapOf(utsMapOf("paddingLeft" to "30rpx")), "pl-4" to padStyleMapOf(utsMapOf("paddingLeft" to "40rpx")), "pl-5" to padStyleMapOf(utsMapOf("paddingLeft" to "50rpx")), "pr-0" to padStyleMapOf(utsMapOf("paddingRight" to 0)), "pr" to padStyleMapOf(utsMapOf("paddingRight" to "5rpx")), "pr-1" to padStyleMapOf(utsMapOf("paddingRight" to "10rpx")), "pr-2" to padStyleMapOf(utsMapOf("paddingRight" to "20rpx")), "pr-3" to padStyleMapOf(utsMapOf("paddingRight" to "30rpx")), "pr-4" to padStyleMapOf(utsMapOf("paddingRight" to "40rpx")), "pr-5" to padStyleMapOf(utsMapOf("paddingRight" to "50rpx")), "py-0" to padStyleMapOf(utsMapOf("paddingTop" to 0, "paddingBottom" to 0)), "py" to padStyleMapOf(utsMapOf("paddingTop" to "5rpx", "paddingBottom" to "5rpx")), "py-1" to padStyleMapOf(utsMapOf("paddingTop" to "10rpx", "paddingBottom" to "10rpx")), "py-2" to padStyleMapOf(utsMapOf("paddingTop" to "20rpx", "paddingBottom" to "20rpx")), "py-3" to padStyleMapOf(utsMapOf("paddingTop" to "30rpx", "paddingBottom" to "30rpx")), "py-4" to padStyleMapOf(utsMapOf("paddingTop" to "40rpx", "paddingBottom" to "40rpx")), "py-5" to padStyleMapOf(utsMapOf("paddingTop" to "50rpx", "paddingBottom" to "50rpx")), "px-0" to padStyleMapOf(utsMapOf("paddingLeft" to 0, "paddingRight" to 0)), "px-1" to padStyleMapOf(utsMapOf("paddingLeft" to "10rpx", "paddingRight" to "10rpx")), "px" to padStyleMapOf(utsMapOf("paddingLeft" to "5rpx", "paddingRight" to "5rpx")), "px-2" to padStyleMapOf(utsMapOf("paddingLeft" to "20rpx", "paddingRight" to "20rpx")), "px-3" to padStyleMapOf(utsMapOf("paddingLeft" to "30rpx", "paddingRight" to "30rpx")), "px-4" to padStyleMapOf(utsMapOf("paddingLeft" to "40rpx", "paddingRight" to "40rpx")), "px-5" to padStyleMapOf(utsMapOf("paddingLeft" to "50rpx", "paddingRight" to "50rpx")), "font-small" to padStyleMapOf(utsMapOf("fontSize" to "20rpx")), "font-sm" to padStyleMapOf(utsMapOf("fontSize" to "25rpx")), "font" to padStyleMapOf(utsMapOf("fontSize" to "30rpx")), "font-md" to padStyleMapOf(utsMapOf("fontSize" to "35rpx")), "font-lg" to padStyleMapOf(utsMapOf("fontSize" to "40rpx")), "h1" to padStyleMapOf(utsMapOf("fontSize" to "80rpx", "lineHeight" to 1.8)), "h2" to padStyleMapOf(utsMapOf("fontSize" to "60rpx", "lineHeight" to 1.8)), "h3" to padStyleMapOf(utsMapOf("fontSize" to "45rpx", "lineHeight" to 1.8)), "h4" to padStyleMapOf(utsMapOf("fontSize" to "32rpx", "lineHeight" to 1.8)), "h5" to padStyleMapOf(utsMapOf("fontSize" to "30rpx", "lineHeight" to 1.8)), "h6" to padStyleMapOf(utsMapOf("fontSize" to "28rpx", "lineHeight" to 1.8)), "text-indent" to padStyleMapOf(utsMapOf("textIndent" to 2)), "text-through" to padStyleMapOf(utsMapOf("textDecoration" to "line-through")), "text-left" to padStyleMapOf(utsMapOf("textAlign" to "left")), "text-right" to padStyleMapOf(utsMapOf("textAlign" to "right")), "text-center" to padStyleMapOf(utsMapOf("textAlign" to "center")), "text-ellipsis" to padStyleMapOf(utsMapOf("overflow" to "hidden", "textOverflow" to "ellipsis", "whiteSpace" to "nowrap")), "font-weight-normal" to padStyleMapOf(utsMapOf("fontWeight" to "400")), "font-weight-bold" to padStyleMapOf(utsMapOf("fontWeight" to "700")), "font-weight-bolder" to padStyleMapOf(utsMapOf("fontWeight" to "bold")), "font-italic" to padStyleMapOf(utsMapOf("fontStyle" to "italic")), "text-white" to padStyleMapOf(utsMapOf("color" to "#ffffff")), "text-primary" to padStyleMapOf(utsMapOf("color" to "#007bff")), "text-hover-primary" to padStyleMapOf(utsMapOf("color" to "#0056b3")), "text-secondary" to padStyleMapOf(utsMapOf("color" to "#6c757d")), "text-hover-secondary" to padStyleMapOf(utsMapOf("color" to "#494f54")), "text-success" to padStyleMapOf(utsMapOf("color" to "#28a745")), "text-hover-success" to padStyleMapOf(utsMapOf("color" to "#19692c")), "text-info" to padStyleMapOf(utsMapOf("color" to "#17a2b8")), "text-hover-info" to padStyleMapOf(utsMapOf("color" to "#0f6674")), "text-warning" to padStyleMapOf(utsMapOf("color" to "#ffc107")), "text-hover-warning" to padStyleMapOf(utsMapOf("color" to "#ba8b00")), "text-danger" to padStyleMapOf(utsMapOf("color" to "#dc3545")), "text-hover-danger" to padStyleMapOf(utsMapOf("color" to "#a71d2a")), "text-light" to padStyleMapOf(utsMapOf("color" to "#f8f9fa")), "text-hover-light" to padStyleMapOf(utsMapOf("color" to "#cbd3da")));
            }
        val styles2: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("text-dark" to padStyleMapOf(utsMapOf("color" to "#343a40")), "text-hover-dark" to padStyleMapOf(utsMapOf("color" to "#121416")), "text-body" to padStyleMapOf(utsMapOf("color" to "#212529")), "text-muted" to padStyleMapOf(utsMapOf("color" to "#6c757d")), "text-light-muted" to padStyleMapOf(utsMapOf("color" to "#A9A5A0")), "text-light-black" to padStyleMapOf(utsMapOf("color" to "rgba(0,0,0,0.5)")), "text-light-white" to padStyleMapOf(utsMapOf("color" to "rgba(255,255,255,0.5)")), "bg-primary" to padStyleMapOf(utsMapOf("backgroundColor" to "#007bff")), "bg-hover-primary" to padStyleMapOf(utsMapOf("backgroundColor:hover" to "#0062cc")), "bg-secondary" to padStyleMapOf(utsMapOf("backgroundColor" to "#6c757d")), "bg-hover-secondary" to padStyleMapOf(utsMapOf("backgroundColor:hover" to "#545b62")), "bg-success" to padStyleMapOf(utsMapOf("backgroundColor" to "#28a745")), "bg-hover-success" to padStyleMapOf(utsMapOf("backgroundColor" to "#1e7e34")), "bg-info" to padStyleMapOf(utsMapOf("backgroundColor" to "#17a2b8")), "bg-hover-info" to padStyleMapOf(utsMapOf("backgroundColor" to "#117a8b")), "bg-warning" to padStyleMapOf(utsMapOf("backgroundColor" to "#ffc107")), "bg-hover-warning" to padStyleMapOf(utsMapOf("backgroundColor" to "#d39e00")), "bg-danger" to padStyleMapOf(utsMapOf("backgroundColor" to "#dc3545")), "bg-hover-danger" to padStyleMapOf(utsMapOf("backgroundColor" to "#bd2130")), "bg-light" to padStyleMapOf(utsMapOf("backgroundColor" to "#f8f9fa")), "bg-hover-light" to padStyleMapOf(utsMapOf("backgroundColor" to "#dae0e5")), "bg-dark" to padStyleMapOf(utsMapOf("backgroundColor" to "#343a40")), "bg-hover-dark" to padStyleMapOf(utsMapOf("backgroundColor" to "#1d2124")), "bg-white" to padStyleMapOf(utsMapOf("backgroundColor" to "#ffffff")), "bg-transparent" to padStyleMapOf(utsMapOf("backgroundColor" to "rgba(0,0,0,0)")), "border" to padStyleMapOf(utsMapOf("borderWidth" to "1rpx", "borderStyle" to "solid", "borderColor" to "#dee2e6")), "border-top" to padStyleMapOf(utsMapOf("borderTopWidth" to "1rpx", "borderTopStyle" to "solid", "borderTopColor" to "#dee2e6")), "border-right" to padStyleMapOf(utsMapOf("borderRightWidth" to "1rpx", "borderRightStyle" to "solid", "borderRightColor" to "#dee2e6")), "border-bottom" to padStyleMapOf(utsMapOf("borderBottomWidth" to "1rpx", "borderBottomStyle" to "solid", "borderBottomColor" to "#dee2e6")), "border-left" to padStyleMapOf(utsMapOf("borderLeftWidth" to "1rpx", "borderLeftStyle" to "solid", "borderLeftColor" to "#dee2e6")), "border-0" to padStyleMapOf(utsMapOf("!borderWidth" to 0)), "border-top-0" to padStyleMapOf(utsMapOf("!borderTopWidth" to 0)), "border-right-0" to padStyleMapOf(utsMapOf("!borderRightWidth" to 0)), "border-bottom-0" to padStyleMapOf(utsMapOf("!borderBottomWidth" to 0)), "border-left-0" to padStyleMapOf(utsMapOf("!borderLeftWidth" to 0)), "border-primary" to padStyleMapOf(utsMapOf("borderColor" to "#007bff")), "border-secondary" to padStyleMapOf(utsMapOf("borderColor" to "#6c757d")), "border-light-secondary" to padStyleMapOf(utsMapOf("borderColor" to "#E9E8E5")), "border-success" to padStyleMapOf(utsMapOf("borderColor" to "#28a745")), "border-info" to padStyleMapOf(utsMapOf("borderColor" to "#17a2b8")), "border-warning" to padStyleMapOf(utsMapOf("borderColor" to "#ffc107")), "border-danger" to padStyleMapOf(utsMapOf("borderColor" to "#dc3545")), "border-light" to padStyleMapOf(utsMapOf("borderColor" to "#f8f9fa")), "border-dark" to padStyleMapOf(utsMapOf("borderColor" to "#343a40")), "border-white" to padStyleMapOf(utsMapOf("borderColor" to "#FFFFFF")), "rounded" to padStyleMapOf(utsMapOf("borderRadius" to "8rpx")), "rounded-top" to padStyleMapOf(utsMapOf("borderTopLeftRadius" to "8rpx", "borderTopRightRadius" to "8rpx")), "rounded-right" to padStyleMapOf(utsMapOf("borderTopRightRadius" to "8rpx", "borderBottomRightRadius" to "8rpx")), "rounded-bottom" to padStyleMapOf(utsMapOf("borderBottomRightRadius" to "8rpx", "borderBottomLeftRadius" to "8rpx")), "rounded-left" to padStyleMapOf(utsMapOf("borderTopLeftRadius" to "8rpx", "borderBottomLeftRadius" to "8rpx")), "rounded-circle" to padStyleMapOf(utsMapOf("borderRadius" to "100rpx")), "rounded-0" to padStyleMapOf(utsMapOf("borderRadius" to 0)), "d-none" to padStyleMapOf(utsMapOf("display" to "none")), "overflow-hidden" to padStyleMapOf(utsMapOf("overflow" to "hidden")), "position-relative" to padStyleMapOf(utsMapOf("position" to "relative")), "position-absolute" to padStyleMapOf(utsMapOf("position" to "absolute")), "position-fixed" to padStyleMapOf(utsMapOf("position" to "fixed")), "fixed-top" to padStyleMapOf(utsMapOf("position" to "fixed", "top" to 0, "right" to 0, "left" to 0, "zIndex" to 1030)), "fixed-bottom" to padStyleMapOf(utsMapOf("position" to "fixed", "right" to 0, "bottom" to 0, "left" to 0, "zIndex" to 1030)), "top-0" to padStyleMapOf(utsMapOf("top" to 0)), "left-0" to padStyleMapOf(utsMapOf("left" to 0)), "right-0" to padStyleMapOf(utsMapOf("right" to 0)), "bottom-0" to padStyleMapOf(utsMapOf("bottom" to 0)), "shadow" to padStyleMapOf(utsMapOf("boxShadow" to "0 2rpx 12rpx rgba(0, 0, 0, 0.15)")), "shadow-lg" to padStyleMapOf(utsMapOf("boxShadow" to "0rpx 40rpx 100rpx 0rpx rgba(0, 0, 0, 0.175)")), "shadow-none" to padStyleMapOf(utsMapOf("!boxShadow" to "none")), "icon-close" to padStyleMapOf(utsMapOf("content:before" to "\"\\e61a\"")), "icon-arrow-top" to padStyleMapOf(utsMapOf("content:before" to "\"\\e605\"")), "icon-arrow-bottom" to padStyleMapOf(utsMapOf("content:before" to "\"\\e606\"")), "icon-playing" to padStyleMapOf(utsMapOf("content:before" to "\"\\e662\"")), "icon-payment" to padStyleMapOf(utsMapOf("content:before" to "\"\\e7af\"")), "icon-add-friend" to padStyleMapOf(utsMapOf("content:before" to "\"\\e64f\"")), "icon-bubble" to padStyleMapOf(utsMapOf("content:before" to "\"\\e957\"")), "icon-scan" to padStyleMapOf(utsMapOf("content:before" to "\"\\e632\"")), "icon-scan-hand" to padStyleMapOf(utsMapOf("content:before" to "\"\\eb5b\"")), "icon-voice" to padStyleMapOf(utsMapOf("content:before" to "\"\\e888\"")), "icon-triangle-right" to padStyleMapOf(utsMapOf("content:before" to "\"\\e653\"")), "icon-triangle-left" to padStyleMapOf(utsMapOf("content:before" to "\"\\e7f5\"")), "icon-emoji" to padStyleMapOf(utsMapOf("content:before" to "\"\\e615\"")), "icon-qrcode" to padStyleMapOf(utsMapOf("content:before" to "\"\\e6bb\"")), "icon-more-dot" to padStyleMapOf(utsMapOf("content:before" to "\"\\e673\"")), "icon-collection-color" to padStyleMapOf(utsMapOf("content:before" to "\"\\e699\"")), "icon-photo-color" to padStyleMapOf(utsMapOf("content:before" to "\"\\e7a4\"")), "icon-call" to padStyleMapOf(utsMapOf("content:before" to "\"\\e659\"")), "icon-arrow-left" to padStyleMapOf(utsMapOf("content:before" to "\"\\e7f4\"")), "icon-search" to padStyleMapOf(utsMapOf("content:before" to "\"\\e7f3\"")), "icon-camera" to padStyleMapOf(utsMapOf("content:before" to "\"\\e603\"")), "icon-collection" to padStyleMapOf(utsMapOf("content:before" to "\"\\e646\"")), "icon-photo" to padStyleMapOf(utsMapOf("content:before" to "\"\\e7f1\"")), "icon-plus-circle" to padStyleMapOf(utsMapOf("content:before" to "\"\\e726\"")), "icon-setting" to padStyleMapOf(utsMapOf("content:before" to "\"\\e668\"")), "icon-arrow-right" to padStyleMapOf(utsMapOf("content:before" to "\"\\e658\"")), "icon-map" to padStyleMapOf(utsMapOf("content:before" to "\"\\e76f\"")), "icon-up-circle" to padStyleMapOf(utsMapOf("content:before" to "\"\\e612\"")), "icon-down-circle" to padStyleMapOf(utsMapOf("content:before" to "\"\\e613\"")), "icon-friends-color" to padStyleMapOf(utsMapOf("content:before" to "\"\\e641\"")), "icon-branchdown-full" to padStyleMapOf(utsMapOf("content:before" to "\"\\e683\"")), "icon-branchdown" to padStyleMapOf(utsMapOf("content:before" to "\"\\e652\"")), "icon-user" to padStyleMapOf(utsMapOf("content:before" to "\"\\e619\"")), "icon-user-fill" to padStyleMapOf(utsMapOf("content:before" to "\"\\e604\"")));
            }
        val styles3: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("icon-grid" to padStyleMapOf(utsMapOf("content:before" to "\"\\e61f\"")), "icon-grid-fill" to padStyleMapOf(utsMapOf("content:before" to "\"\\e602\"")), "icon-chat-fill" to padStyleMapOf(utsMapOf("content:before" to "\"\\e601\"")), "icon-chat" to padStyleMapOf(utsMapOf("content:before" to "\"\\e600\"")), "@FONT-FACE" to utsMapOf("0" to utsMapOf("fontFamily" to "iconfont", "src" to "url('iconfont.woff2?t=1730651033965') format('woff2'),\r\n       url('iconfont.woff?t=1730651033965') format('woff'),\r\n       url('/static/iconfont.ttf?t=1730651033965') format('truetype')")));
            }
    }
}
val GenAppClass = CreateVueAppComponent(GenApp::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "app", name = "", inheritAttrs = true, inject = Map(), props = Map(), propsNeedCastKeys = utsArrayOf(), emits = Map(), components = Map(), styles = GenApp.styles);
}
, fun(instance): GenApp {
    return GenApp(instance);
}
);
val GenCompomentsGviewGIconButtonClass = CreateVueComponent(GenCompomentsGviewGIconButton::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenCompomentsGviewGIconButton.inheritAttrs, inject = GenCompomentsGviewGIconButton.inject, props = GenCompomentsGviewGIconButton.props, propsNeedCastKeys = GenCompomentsGviewGIconButton.propsNeedCastKeys, emits = GenCompomentsGviewGIconButton.emits, components = GenCompomentsGviewGIconButton.components, styles = GenCompomentsGviewGIconButton.styles, setup = fun(props: ComponentPublicInstance): Any? {
        return GenCompomentsGviewGIconButton.setup(props as GenCompomentsGviewGIconButton);
    }
    );
}
, fun(instance): GenCompomentsGviewGIconButton {
    return GenCompomentsGviewGIconButton(instance);
}
);
val GenCompomentsGviewGNavbarClass = CreateVueComponent(GenCompomentsGviewGNavbar::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenCompomentsGviewGNavbar.inheritAttrs, inject = GenCompomentsGviewGNavbar.inject, props = GenCompomentsGviewGNavbar.props, propsNeedCastKeys = GenCompomentsGviewGNavbar.propsNeedCastKeys, emits = GenCompomentsGviewGNavbar.emits, components = GenCompomentsGviewGNavbar.components, styles = GenCompomentsGviewGNavbar.styles, setup = fun(props: ComponentPublicInstance): Any? {
        return GenCompomentsGviewGNavbar.setup(props as GenCompomentsGviewGNavbar);
    }
    );
}
, fun(instance): GenCompomentsGviewGNavbar {
    return GenCompomentsGviewGNavbar(instance);
}
);
val GenPagesTabbarIndexIndexClass = CreateVueComponent(GenPagesTabbarIndexIndex::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesTabbarIndexIndex.inheritAttrs, inject = GenPagesTabbarIndexIndex.inject, props = GenPagesTabbarIndexIndex.props, propsNeedCastKeys = GenPagesTabbarIndexIndex.propsNeedCastKeys, emits = GenPagesTabbarIndexIndex.emits, components = GenPagesTabbarIndexIndex.components, styles = GenPagesTabbarIndexIndex.styles, setup = fun(props: ComponentPublicInstance): Any? {
        return GenPagesTabbarIndexIndex.setup(props as GenPagesTabbarIndexIndex);
    }
    );
}
, fun(instance): GenPagesTabbarIndexIndex {
    return GenPagesTabbarIndexIndex(instance);
}
);
val GenPagesTabbarAddressbookAddressbookClass = CreateVueComponent(GenPagesTabbarAddressbookAddressbook::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesTabbarAddressbookAddressbook.inheritAttrs, inject = GenPagesTabbarAddressbookAddressbook.inject, props = GenPagesTabbarAddressbookAddressbook.props, propsNeedCastKeys = GenPagesTabbarAddressbookAddressbook.propsNeedCastKeys, emits = GenPagesTabbarAddressbookAddressbook.emits, components = GenPagesTabbarAddressbookAddressbook.components, styles = GenPagesTabbarAddressbookAddressbook.styles);
}
, fun(instance): GenPagesTabbarAddressbookAddressbook {
    return GenPagesTabbarAddressbookAddressbook(instance);
}
);
val GenPagesTabbarFindFindClass = CreateVueComponent(GenPagesTabbarFindFind::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesTabbarFindFind.inheritAttrs, inject = GenPagesTabbarFindFind.inject, props = GenPagesTabbarFindFind.props, propsNeedCastKeys = GenPagesTabbarFindFind.propsNeedCastKeys, emits = GenPagesTabbarFindFind.emits, components = GenPagesTabbarFindFind.components, styles = GenPagesTabbarFindFind.styles);
}
, fun(instance): GenPagesTabbarFindFind {
    return GenPagesTabbarFindFind(instance);
}
);
val GenCompomentsGviewGListitemClass = CreateVueComponent(GenCompomentsGviewGListitem::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenCompomentsGviewGListitem.inheritAttrs, inject = GenCompomentsGviewGListitem.inject, props = GenCompomentsGviewGListitem.props, propsNeedCastKeys = GenCompomentsGviewGListitem.propsNeedCastKeys, emits = GenCompomentsGviewGListitem.emits, components = GenCompomentsGviewGListitem.components, styles = GenCompomentsGviewGListitem.styles, setup = fun(props: ComponentPublicInstance): Any? {
        return GenCompomentsGviewGListitem.setup(props as GenCompomentsGviewGListitem);
    }
    );
}
, fun(instance): GenCompomentsGviewGListitem {
    return GenCompomentsGviewGListitem(instance);
}
);
val GenCompomentsGviewGDividerClass = CreateVueComponent(GenCompomentsGviewGDivider::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "component", name = "", inheritAttrs = GenCompomentsGviewGDivider.inheritAttrs, inject = GenCompomentsGviewGDivider.inject, props = GenCompomentsGviewGDivider.props, propsNeedCastKeys = GenCompomentsGviewGDivider.propsNeedCastKeys, emits = GenCompomentsGviewGDivider.emits, components = GenCompomentsGviewGDivider.components, styles = GenCompomentsGviewGDivider.styles);
}
, fun(instance): GenCompomentsGviewGDivider {
    return GenCompomentsGviewGDivider(instance);
}
);
val GenPagesTabbarMyMyClass = CreateVueComponent(GenPagesTabbarMyMy::class.java, fun(): VueComponentOptions {
    return VueComponentOptions(type = "page", name = "", inheritAttrs = GenPagesTabbarMyMy.inheritAttrs, inject = GenPagesTabbarMyMy.inject, props = GenPagesTabbarMyMy.props, propsNeedCastKeys = GenPagesTabbarMyMy.propsNeedCastKeys, emits = GenPagesTabbarMyMy.emits, components = GenPagesTabbarMyMy.components, styles = GenPagesTabbarMyMy.styles, setup = fun(props: ComponentPublicInstance): Any? {
        return GenPagesTabbarMyMy.setup(props as GenPagesTabbarMyMy);
    }
    );
}
, fun(instance): GenPagesTabbarMyMy {
    return GenPagesTabbarMyMy(instance);
}
);
fun createApp(): UTSJSONObject {
    val app = createSSRApp(GenAppClass);
    return UTSJSONObject(Map<String, Any?>(utsArrayOf(
        utsArrayOf(
            "app",
            app
        )
    )));
}
fun main(app: IApp) {
    definePageRoutes();
    defineAppConfig();
    (createApp()["app"] as VueApp).mount(app);
}
open class UniAppConfig : io.dcloud.uniapp.appframe.AppConfig {
    override var name: String = "BG-uniappx";
    override var appid: String = "";
    override var versionName: String = "1.0.0";
    override var versionCode: String = "100";
    override var uniCompilerVersion: String = "4.29";
    constructor() : super() {}
}
fun definePageRoutes() {
    __uniRoutes.push(UniPageRoute(path = "pages/tabbar/index/index", component = GenPagesTabbarIndexIndexClass, meta = UniPageMeta(isQuit = true), style = utsMapOf()));
    __uniRoutes.push(UniPageRoute(path = "pages/tabbar/addressbook/addressbook", component = GenPagesTabbarAddressbookAddressbookClass, meta = UniPageMeta(isQuit = false), style = utsMapOf()));
    __uniRoutes.push(UniPageRoute(path = "pages/tabbar/find/find", component = GenPagesTabbarFindFindClass, meta = UniPageMeta(isQuit = false), style = utsMapOf()));
    __uniRoutes.push(UniPageRoute(path = "pages/tabbar/my/my", component = GenPagesTabbarMyMyClass, meta = UniPageMeta(isQuit = false), style = utsMapOf()));
}
val __uniTabBar: Map<String, Any?>? = utsMapOf("color" to "#454648", "selectedColor" to "#427CE8", "borderStyle" to "black", "backgroundColor" to "#FAFAFA", "list" to utsArrayOf(
    utsMapOf("iconPath" to "static/tabbar/mail.png", "selectedIconPath" to "static/tabbar/mail-select.png", "pagePath" to "pages/tabbar/index/index", "text" to "消息"),
    utsMapOf("iconPath" to "static/tabbar/addressbook.png", "selectedIconPath" to "static/tabbar/addressbook-select.png", "pagePath" to "pages/tabbar/addressbook/addressbook", "text" to "通讯录"),
    utsMapOf("iconPath" to "static/tabbar/find.png", "selectedIconPath" to "static/tabbar/find-select.png", "pagePath" to "pages/tabbar/find/find", "text" to "工作台"),
    utsMapOf("iconPath" to "static/tabbar/my.png", "selectedIconPath" to "static/tabbar/my-select.png", "pagePath" to "pages/tabbar/my/my", "text" to "我")
));
val __uniLaunchPage: Map<String, Any?> = utsMapOf("url" to "pages/tabbar/index/index", "style" to utsMapOf<String, Any?>());
fun defineAppConfig() {
    __uniConfig.entryPagePath = "/pages/tabbar/index/index";
    __uniConfig.globalStyle = utsMapOf("navigationBarTextStyle" to "black", "navigationBarTitleText" to "uni-app x", "navigationBarBackgroundColor" to "#F8F8F8", "backgroundColor" to "#F8F8F8", "navigationStyle" to "custom");
    __uniConfig.tabBar = __uniTabBar as Map<String, Any>?;
    __uniConfig.conditionUrl = "";
    __uniConfig.uniIdRouter = utsMapOf();
    __uniConfig.ready = true;
}
fun getApp(): GenApp {
    return getBaseApp() as GenApp;
}
