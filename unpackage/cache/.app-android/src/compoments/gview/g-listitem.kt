@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME", "UNUSED_ANONYMOUS_PARAMETER")
package uni.UNIuniappx;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenCompomentsGviewGListitem : VueComponent {
    constructor(__ins: ComponentInternalInstance) : super(__ins) {}
    open var imgUrl: String by `$props`;
    open var title: String by `$props`;
    open var isShowLeft: Boolean by `$props`;
    open var isShowRight: Boolean by `$props`;
    open var imageSize: Number by `$props`;
    open var isShowIcon: Boolean by `$props`;
    open var isShowRightIcon: Boolean by `$props`;
    companion object {
        @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
        var setup: (__props: GenCompomentsGviewGListitem) -> Any? = fun(__props): Any? {
            val __ins = getCurrentInstance()!!;
            val _ctx = __ins.proxy as GenCompomentsGviewGListitem;
            val _cache = __ins.renderCache;
            val props = __props;
            val getImageStyle = computed(fun(): String {
                return "width:" + props.imageSize + "rpx;height:" + props.imageSize + "rpx";
            }
            );
            return fun(): Any? {
                return createElementVNode("view", utsMapOf("class" to "bg-white flex align-stretch", "hover-class" to "bg-light"), utsArrayOf(
                    if (isTrue(_ctx.isShowLeft)) {
                        createElementVNode("view", utsMapOf("key" to 0, "class" to "flex align-center pr-2"), utsArrayOf(
                            renderSlot(_ctx.`$slots`, "leftCtx")
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    }
                    ,
                    if (isTrue(_ctx.isShowIcon)) {
                        createElementVNode("view", utsMapOf("key" to 1, "class" to "flex align-center justify-center py-2 pl-3"), utsArrayOf(
                            renderSlot(_ctx.`$slots`, "icon"),
                            if (isTrue(props.imgUrl)) {
                                createElementVNode("image", utsMapOf("key" to 0, "src" to props.imgUrl, "mode" to "widthFix", "style" to normalizeStyle(getImageStyle.value)), null, 12, utsArrayOf(
                                    "src"
                                ));
                            } else {
                                createCommentVNode("v-if", true);
                            }
                        ));
                    } else {
                        createCommentVNode("v-if", true);
                    }
                    ,
                    createElementVNode("view", utsMapOf("class" to "flex-1 border-bottom flex align-center justify-between pr-1 py-3 pl-3"), utsArrayOf(
                        renderSlot(_ctx.`$slots`, "default", UTSJSONObject(), fun(): UTSArray<Any> {
                            return utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "font-md text-dark"), toDisplayString(props.title), 1)
                            );
                        }
                        ),
                        if (isTrue(_ctx.isShowRight)) {
                            createElementVNode("view", utsMapOf("key" to 0, "class" to "flex align-center pr-2"), utsArrayOf(
                                renderSlot(_ctx.`$slots`, "rightCtx", utsMapOf("class" to "")),
                                if (isTrue(_ctx.isShowRightIcon)) {
                                    createElementVNode("text", utsMapOf("key" to 0, "class" to "iconfont font-md text-light-muted ml-1 icon-arrow-right"));
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                            ));
                        } else {
                            createCommentVNode("v-if", true);
                        }
                    ))
                ));
            }
            ;
        }
        ;
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf());
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("imgUrl" to utsMapOf("type" to "String", "required" to true, "default" to ""), "title" to utsMapOf("type" to "String", "required" to true, "default" to ""), "isShowLeft" to utsMapOf("type" to "Boolean", "required" to true, "default" to false), "isShowRight" to utsMapOf("type" to "Boolean", "required" to true, "default" to false), "imageSize" to utsMapOf("type" to "Number", "required" to true, "default" to 75), "isShowIcon" to utsMapOf("type" to "Boolean", "required" to true, "default" to true), "isShowRightIcon" to utsMapOf("type" to "Boolean", "required" to true, "default" to true)));
        var propsNeedCastKeys = utsArrayOf(
            "imgUrl",
            "title",
            "isShowLeft",
            "isShowRight",
            "imageSize",
            "isShowIcon",
            "isShowRightIcon"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
