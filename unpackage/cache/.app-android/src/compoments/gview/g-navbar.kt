@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME", "UNUSED_ANONYMOUS_PARAMETER")
package uni.UNIuniappx;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
import io.dcloud.uniapp.extapi.getSystemInfoSync as uni_getSystemInfoSync;
import io.dcloud.uniapp.extapi.navigateBack as uni_navigateBack;
import io.dcloud.uniapp.extapi.rpx2px as uni_rpx2px;
open class GenCompomentsGviewGNavbar : VueComponent {
    constructor(__ins: ComponentInternalInstance) : super(__ins) {}
    open var title: String by `$props`;
    open var fixed: Boolean by `$props`;
    open var noreadnum: Number by `$props`;
    open var bgColor: String by `$props`;
    open var isShowBack: Boolean by `$props`;
    open var isShowRight: Boolean by `$props`;
    open var textColor: String by `$props`;
    companion object {
        @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
        var setup: (__props: GenCompomentsGviewGNavbar) -> Any? = fun(__props): Any? {
            val __ins = getCurrentInstance()!!;
            val _ctx = __ins.proxy as GenCompomentsGviewGNavbar;
            val _cache = __ins.renderCache;
            val props = __props;
            val statusBarHeight = ref(0);
            val navHeight = ref(90);
            val extendPosition = ref(object : UTSJSONObject() {
                var X = uni_rpx2px(735)
                var Y = uni_rpx2px(160)
            });
            onMounted(fun(){
                statusBarHeight.value = uni_getSystemInfoSync().statusBarHeight;
            }
            );
            val fixedStyle = computed(fun(): String {
                return "height:" + statusBarHeight.value + uni_rpx2px(navHeight.value) + "px";
            }
            );
            val getTitle = computed(fun(): String {
                var noreadnum = if (props.noreadnum > 0) {
                    "(" + props.noreadnum + ")";
                } else {
                    "";
                }
                ;
                return "" + props.title + noreadnum;
            }
            );
            val backToPreviousPage = fun(){
                uni_navigateBack(NavigateBackOptions(delta = 1));
            }
            ;
            return fun(): Any? {
                return createElementVNode("view", null, utsArrayOf(
                    createElementVNode("view", utsMapOf("class" to normalizeClass(utsArrayOf(
                        _ctx.bgColor,
                        if (props.fixed) {
                            "fixed-top";
                        } else {
                            "";
                        }
                    ))), utsArrayOf(
                        createElementVNode("view", utsMapOf("class" to "status_bar")),
                        createElementVNode("view", utsMapOf("class" to "w-100 flex justify-between align-center position-relative", "style" to normalizeStyle("height: " + navHeight.value + "rpx")), utsArrayOf(
                            createElementVNode("view", utsMapOf("class" to "flex align-center"), utsArrayOf(
                                if (isTrue(_ctx.isShowBack)) {
                                    createVNode(unref(GenCompomentsGviewGIconButtonClass), utsMapOf("key" to 0, "color" to _ctx.textColor, "onClick" to backToPreviousPage, "icon" to "\ue7f4"), null, 8, utsArrayOf(
                                        "color"
                                    ));
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                            )),
                            createElementVNode("view", utsMapOf("class" to "position-absolute", "style" to normalizeStyle(utsMapOf("left" to "50%", "transform" to "translateX(-50%)"))), utsArrayOf(
                                createElementVNode("text", utsMapOf("class" to "text-white font-md"), toDisplayString(getTitle.value), 1)
                            ), 4),
                            createElementVNode("view", utsMapOf("class" to "flex align-center"), utsArrayOf(
                                if (isTrue(_ctx.isShowRight)) {
                                    renderSlot(_ctx.`$slots`, "right", utsMapOf("key" to 0), fun(): UTSArray<Any> {
                                        return utsArrayOf(
                                            createVNode(unref(GenCompomentsGviewGIconButtonClass), utsMapOf("icon" to "\ue7f3")),
                                            createVNode(unref(GenCompomentsGviewGIconButtonClass), utsMapOf("icon" to "\ue726"))
                                        );
                                    });
                                } else {
                                    createCommentVNode("v-if", true);
                                }
                            ))
                        ), 4)
                    ), 2)
                ));
            }
            ;
        }
        ;
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf(
                    styles0
                ));
            }
        val styles0: Map<String, Map<String, Map<String, Any>>>
            get() {
                return utsMapOf("status_bar" to padStyleMapOf(utsMapOf("height" to CSS_VAR_STATUS_BAR_HEIGHT, "width" to "100%")));
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf("search" to null);
        var props = normalizePropsOptions(utsMapOf("title" to utsMapOf("type" to "String", "required" to true, "default" to ""), "fixed" to utsMapOf("type" to "Boolean", "required" to true, "default" to true), "noreadnum" to utsMapOf("type" to "Number", "required" to true, "default" to 0), "bgColor" to utsMapOf("type" to "String", "required" to true, "default" to "navbar-bgcolor"), "isShowBack" to utsMapOf("type" to "Boolean", "required" to true, "default" to false), "isShowRight" to utsMapOf("type" to "Boolean", "required" to true, "default" to true), "textColor" to utsMapOf("type" to "String", "required" to true, "default" to "white")));
        var propsNeedCastKeys = utsArrayOf(
            "title",
            "fixed",
            "noreadnum",
            "bgColor",
            "isShowBack",
            "isShowRight",
            "textColor"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
