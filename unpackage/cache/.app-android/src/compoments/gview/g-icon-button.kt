@file:Suppress("UNCHECKED_CAST", "USELESS_CAST", "INAPPLICABLE_JVM_NAME", "UNUSED_ANONYMOUS_PARAMETER")
package uni.UNIuniappx;
import io.dcloud.uniapp.*;
import io.dcloud.uniapp.extapi.*;
import io.dcloud.uniapp.framework.*;
import io.dcloud.uniapp.runtime.*;
import io.dcloud.uniapp.vue.*;
import io.dcloud.uniapp.vue.shared.*;
import io.dcloud.unicloud.*;
import io.dcloud.uts.*;
import io.dcloud.uts.Map;
import io.dcloud.uts.Set;
import io.dcloud.uts.UTSAndroid;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.async;
open class GenCompomentsGviewGIconButton : VueComponent {
    constructor(__ins: ComponentInternalInstance) : super(__ins) {}
    open var icon: String? by `$props`;
    open var color: String by `$props`;
    open var size: Number by `$props`;
    companion object {
        @Suppress("UNUSED_PARAMETER", "UNUSED_VARIABLE")
        var setup: (__props: GenCompomentsGviewGIconButton) -> Any? = fun(__props): Any? {
            val __ins = getCurrentInstance()!!;
            val _ctx = __ins.proxy as GenCompomentsGviewGIconButton;
            val _cache = __ins.renderCache;
            val props = __props;
            val getIconStyle = computed(fun(): String {
                var iconStyle = "color:" + props.color + ";font-size:" + props.size + "rpx";
                return iconStyle;
            }
            );
            return fun(): Any? {
                return createElementVNode("view", utsMapOf("class" to "flex justify-center align-center", "style" to normalizeStyle(utsMapOf("height" to "90rpx", "width" to "90rpx"))), utsArrayOf(
                    createElementVNode("text", utsMapOf("class" to "iconfont", "style" to normalizeStyle(getIconStyle.value)), toDisplayString(props.icon), 5)
                ), 4);
            }
            ;
        }
        ;
        val styles: Map<String, Map<String, Map<String, Any>>>
            get() {
                return normalizeCssStyles(utsArrayOf());
            }
        var inheritAttrs = true;
        var inject: Map<String, Map<String, Any?>> = utsMapOf();
        var emits: Map<String, Any?> = utsMapOf();
        var props = normalizePropsOptions(utsMapOf("icon" to utsMapOf("type" to "String", "required" to false), "color" to utsMapOf("type" to "String", "required" to true, "default" to "#FFF"), "size" to utsMapOf("type" to "Number", "required" to true, "default" to 35)));
        var propsNeedCastKeys = utsArrayOf(
            "color",
            "size"
        );
        var components: Map<String, CreateVueComponent> = utsMapOf();
    }
}
